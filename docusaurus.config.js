// @ts-check
// `@type` JSDoc annotations allow editor autocompletion and type checking
// (when paired with `@ts-check`).
// There are various equivalent ways to declare your Docusaurus config.
// See: https://docusaurus.io/docs/api/docusaurus-config

import {themes as prismThemes} from 'prism-react-renderer';

// Define a date to show last time the blog was updated:
const currentDate = new Date();
const options = { month: 'long', day: 'numeric', year: 'numeric' };
const formattedDate = `Last updated on ${currentDate.toLocaleDateString('en-US', options)}`;

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'CUSTOM CODE',
  tagline: 'Kubernetes, Bitcoin L2s, EDGE DERMS, AI agents, MLOps and more',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: 'https://your-docusaurus-site.example.com',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'code', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

// presets: ['classic', /** @type {import('@docusaurus/preset-classic').Options} */],  
  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: './sidebars.js',
          // Please change this to your repo.
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
        },
        theme: {
          customCss: './src/css/custom.css',
        },
      }),
    ],
  ],

  plugins: [
    [
      '@docusaurus/plugin-content-blog',
      {
        /**
         * Required for any multi-instance plugin
         */
        id: 'kubernetes',
        /**
         * URL route for the blog section of your site.
         * *DO NOT* include a trailing slash.
         */
        routeBasePath: 'kubernetes',
        /**
         * Path to data on filesystem relative to site dir.
         */
        path: './kubernetes',
      },
    ],
    [
      '@docusaurus/plugin-content-blog',
      {
        /**
         * Required for any multi-instance plugin
         */
        id: 'osed',
        /**
         * URL route for the blog section of your site.
         * *DO NOT* include a trailing slash.
         */
        routeBasePath: 'osed',
        /**
         * Path to data on filesystem relative to site dir.
         */
        path: './osed',
      },
    ],
    [
      '@docusaurus/plugin-content-blog',
      {
        /**
         * Required for any multi-instance plugin
         */
        id: 'bitcoin',
        /**
         * URL route for the blog section of your site.
         * *DO NOT* include a trailing slash.
         */
        routeBasePath: 'bitcoin',
        /**
         * Path to data on filesystem relative to site dir.
         */
        path: './bitcoin',
      },
    ],
    [
      '@docusaurus/plugin-content-blog',
      {
        /**
         * Required for any multi-instance plugin
         */
        id: 'mlops',
        /**
         * URL route for the blog section of your site.
         * *DO NOT* include a trailing slash.
         */
        routeBasePath: 'mlops',
        /**
         * Path to data on filesystem relative to site dir.
         */
        path: './mlops',
      },
    ],    
    [
      '@docusaurus/plugin-content-blog',
      {
        /**
         * Required for any multi-instance plugin
         */
        id: 'game',
        /**
         * URL route for the blog section of your site.
         * *DO NOT* include a trailing slash.
         */
        routeBasePath: 'game',
        /**
         * Path to data on filesystem relative to site dir.
         */
        path: './game',
      },
    ],    
  ],
  
  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/docusaurus-social-card.jpg',
      navbar: {
        title: 'Home',
        logo: {
          alt: 'My Site Logo',
          src: 'img/logo.svg',
        },
        items: [          
          {to: '/kubernetes', label: 'Kubernetes', position: 'left'},
          {to: '/osed', label: 'OSED', position: 'left'},
          {to: '/bitcoin', label: 'Bitcoin L2', position: 'left'},
          {to: '/mlops', label: 'MLOps', position: 'left'},
          { // Use this for a page with the docs style
            type: 'docSidebar',
            sidebarId: 'cvSidebar',
            position: 'left',
            label: 'Docs',
          },
          {to: '/game', label: 'The game', position: 'left'},
          // {
            //   to: 'pathname:///kubernetes',  // Replace './sphinx' with the actual path
          //   label: 'Kubernetes',
          //   position: 'left',
          // },
          {
            href: 'https://www.linkedin.com/in/jcoc/',
            label: 'LinkedIn',
            position: 'right',
          },
          {
            href: 'https://twitter.com/Juancaoviedo',
            label: 'Twitter',
            position: 'right',
          },
          {
            href: 'https://gitlab.com/Juancaoviedo',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        // links: [
        //   {
        //     items: [
        //       {
        //         label: 'GitLab',
        //         href: 'https://gitlab.com/Juancaoviedo',
        //       },
        //   ]
        //   },
        //   {
        //     items: [
        //       {
        //         label: 'Twitter - X',
        //         href: 'https://twitter.com/Juancaoviedo',
        //       },
        //   ]
        //   },
        //   {
        //     items: [
        //       {
        //         label: 'LinkedIn',
        //         href: 'https://www.linkedin.com/in/jcoc/',
        //       },
        //   ]
        //   },


        //   // {
        //   //   title: 'Pages',
        //   //   items: [
        //   //     {
        //   //       label: 'Kubernetes',
        //   //       to: '/kubernetes',
        //   //     },
        //   //     {
        //   //       label: 'OSED',
        //   //       to: '/osed',
        //   //     },
        //   //     {
        //   //       label: 'Bitcoin L2',
        //   //       to: '/bitcoin',
        //   //     },
        //   //     {
        //   //       label: 'MLOps',
        //   //       to: '/mlops',
        //   //     },
        //   //     {
        //   //       label: 'CV',
        //   //       to: '/docs',
        //   //     },
        //   //     {
        //   //       label: 'The game',
        //   //       to: '/game',
        //   //     },
        //   //   ],
        //   // },
        //   // {
        //   //   title: 'Community',
        //   //   items: [
        //   //     {
        //   //       label: 'LinkedIn',
        //   //       href: 'https://www.linkedin.com/in/jcoc/',
        //   //     },
        //   //     {
        //   //       label: 'Twitter',
        //   //       href: 'https://twitter.com/Juancaoviedo',
        //   //     },
        //   //   ],
        //   // },
        //   // {
        //   //   title: 'More',
        //   //   items: [
        //   //     // {
        //   //     //   label: 'Blog',
        //   //     //   to: '/blog',
        //   //     // },
        //   //     {
        //   //       label: 'GitLab',
        //   //       href: 'https://gitlab.com/Juancaoviedo',
        //   //     },
        //   //   ],
        //   // },
        // ],
        copyright: `Built with Docusaurus in my spare time. ${formattedDate}`,
      },
      prism: {
        theme: prismThemes.github,
        darkTheme: prismThemes.dracula,
      },
    }),
};

export default config;
