---
slug: welcome
title: Exploring Bitcoin Layer 2 Scaling Solutions
authors: [juan]
tags: [bitcoin, layer-2, scaling, experiments]
---

Welcome to my blog dedicated to exploring Bitcoin Layer 2 scaling solutions! Here, I delve into various experiments and techniques aimed at enhancing the scalability of Bitcoin using Layer 2 solutions. Each experiment will be documented in the form of a blog post, accompanied by the relevant code hosted on GitLab. Whether you have questions, suggestions, or are interested in collaboration, please don't hesitate to reach out. Let's innovate together in the world of Bitcoin scalability!


<!-- [Docusaurus blogging features](https://docusaurus.io/docs/blog) are powered by the [blog plugin](https://docusaurus.io/docs/api/plugins/@docusaurus/plugin-content-blog).

Simply add Markdown files (or folders) to the `blog` directory.

Regular blog authors can be added to `authors.yml`.

The blog post date can be extracted from filenames, such as:

- `2019-05-30-welcome.md`
- `2019-05-30-welcome/index.md`

A blog post folder can be convenient to co-locate blog post images:


The blog supports tags as well!

**And if you don't want a blog**: just delete this directory, and use `blog: false` in your Docusaurus config. -->
